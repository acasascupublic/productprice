package com.company.pricesearch.infrastructure.persistence.repository;


import com.company.pricesearch.infrastructure.persistence.entity.Price;
import java.time.LocalDateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PriceSearchRepository extends JpaRepository<Price, Long> {

  @Query("SELECT p FROM Price p WHERE p.productId = :productId AND p.brandId = :brandId "
      + "AND p.startDate <= :applicationDateTime AND p.endDate >= :applicationDateTime"
      + " ORDER BY p.priority DESC")
  Page<Price> findPricesByDateTime(Integer productId, Integer brandId,
      LocalDateTime applicationDateTime, Pageable pageable);

}
