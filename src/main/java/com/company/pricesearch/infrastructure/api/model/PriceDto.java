package com.company.pricesearch.infrastructure.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PriceDto implements Serializable {

  @JsonProperty("product")
  private Integer productId;

  @JsonProperty("brand")
  private Integer brandId;

  @JsonProperty("price_rate")
  private Integer priceRate;

  @JsonProperty("start_date")
  private LocalDateTime startDate;

  @JsonProperty("end_date")
  private LocalDateTime endDate;

  @JsonProperty("price")
  private BigDecimal price;

  @JsonProperty("currency")
  private String curr;
}
