package com.company.pricesearch.application.mapper;

import com.company.pricesearch.infrastructure.api.model.PriceDto;
import com.company.pricesearch.infrastructure.persistence.entity.Price;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PriceMapper {

  PriceDto toPriceDto(Price price);
}
