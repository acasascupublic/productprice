package com.company.pricesearch.application.service;

import com.company.pricesearch.infrastructure.api.model.PriceDto;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public interface PriceSearchService {

   Optional<PriceDto> getFirstPriceByDateTime(Integer productId, Integer brandId,
      LocalDateTime applicationDateTime);

}
