package com.company.pricesearch.application.service;

import com.company.pricesearch.application.mapper.PriceMapper;
import com.company.pricesearch.infrastructure.api.model.PriceDto;
import com.company.pricesearch.infrastructure.persistence.repository.PriceSearchRepository;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class PriceSearchServiceImpl implements PriceSearchService{
  @Autowired
  private PriceSearchRepository priceSearchRepository;
  @Autowired
  private PriceMapper priceMapper;

  public Optional<PriceDto> getFirstPriceByDateTime(Integer productId, Integer brandId,
      LocalDateTime applicationDateTime) {
    return
        priceSearchRepository.findPricesByDateTime(
                productId, brandId, applicationDateTime, PageRequest.of(0, 1))
            .map(priceMapper::toPriceDto).stream().findFirst();
  }
}
